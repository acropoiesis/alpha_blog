I made this site using Ruby on Rails for the back end, with PostgreSQL for the database. For the front end, I used a bit of Bootstrap and CSS with minimal Sass. I also decided to use Haml for all templates instead of erb.

http://alpha-blog-michael-nicholas.herokuapp.com/
