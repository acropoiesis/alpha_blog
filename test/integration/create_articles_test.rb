require 'test_helper'

class CreateArticlesTest < ActionDispatch::IntegrationTest

  test "create a new article" do
    get new_article_path
    assert_template 'articles/new'
    assert_difference 'Article.count', 1 do
      post_via_redirect '/articles', article: { title: "An article", description: "This is an article" }
    end
    assert_template 'articles/show'
    assert_match "An article", response.body
    assert_match "This is an article", response.body
  end

end
